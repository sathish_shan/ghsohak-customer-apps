// import React from 'react';
// import {View} from 'react-native';
// import {WebView} from 'react-native-webview';

// const App = () => {
//   return (
//     <View style={{height: '100%'}}>
//       <WebView
//         source={{uri: 'https://vijayalakshmistore.ghoshak.store'}}
//         style={{height: '100%'}}
//       />
//     </View>
//   );
// };
// export default App;


import React, {useRef, useState, useCallback, useEffect} from 'react';
import {BackHandler} from 'react-native';
import {WebView} from 'react-native-webview';

export default function App() {
  const webView = useRef(null);
  const [canGoBack, setCanGoBack] = useState(false);
  const handleBack = useCallback(() => {
    if (canGoBack && webView.current) {
      webView.current.goBack();
      return true;
    }
    return false;
  }, [canGoBack]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
    };
  }, [handleBack]);

  return (
    <WebView
      ref={webView}
      source={{uri: 'https://vijayalakshmistore.ghoshak.store'}}
      onLoadProgress={event => setCanGoBack(event.nativeEvent.canGoBack)}
    />
  );
}