import React from 'react';
import {View} from 'react-native';
import {WebView} from 'react-native-webview';

const App = () => {
  return (
    <View style={{height: '100%'}}>
      <WebView
        source={{uri: 'https://ishanmart.ghoshak.store'}}
        style={{height: '100%'}}
      />
    </View>
  );
};
export default App;
