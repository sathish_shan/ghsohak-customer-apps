import React, {useRef, useState, useCallback, useEffect} from 'react';
import {Alert, BackHandler} from 'react-native';
import {WebView} from 'react-native-webview';

export default function App() {
  const webView = useRef(null);
  const [canGoBack, setCanGoBack] = useState(false);
  const handleBack = useCallback(() => {
    if (canGoBack && webView.current) {
      webView.current.goBack();
      return true;
    } else {
      Alert.alert(
        'DO YOU WANT TO EXIT',
        'Click Exit',
        [
          {
            text: 'CANCEL',
            onPress: () => {
              return true;
            },
          },
          {
            text: 'EXIT',
            onPress: () => {
              BackHandler.exitApp();
            },
          },
        ],
        {cancelable: true},
      );
    }
    return true;
  }, [canGoBack]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
    };
  }, [handleBack]);

  return (
    <WebView
      ref={webView}
      source={{uri: 'https://alayacards.ghoshak.store'}}
      onLoadProgress={event => setCanGoBack(event.nativeEvent.canGoBack)}
    />
  );
}